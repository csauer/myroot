import os, sys
project_dir = os.path.abspath(os.path.join(os.path.dirname(__file__)))
sys.path.insert(0, os.path.join(project_dir, ".."))

# Create a FileIo class for ROOT files
import hep.root.fio
root_io = hep.root.fio.FileIo(os.path.join(project_dir, "out"))

# Add some files
root_io.addFile("input.root")
root_io.addFile("output.root")

# Create some root object
import ROOT
t_h1 = ROOT.TH1F("h1", "", 100, -1, 1)
t_h2 = ROOT.TH1F("h2", "", 100, -1, 1)

# Add to root files
root_io.addObj(t_h1, "input.root", directory="in/put")
root_io.addObj(t_h2, "input.root")

# Get this object from file
t_h1_out1 = root_io.getObj("h1", "input", directory="in/put")
t_h1_out2 = root_io.getObj("h1", "input", directory="in")
t_h2_out  = root_io.getObj("h2", "input")
print(t_h1_out1, t_h1_out2, t_h2_out)
