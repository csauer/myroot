import os, sys
project_dir = os.path.abspath(os.path.join(os.path.dirname(__file__)))
sys.path.insert(0, os.path.join(project_dir, ".."))

import numpy as np
import ROOT
import hep.plt.utils

# initialize histogram
t_h = ROOT.TH1D("gauss", "", 50, -5, 5)

# Get data
x = np.random.normal(0, 1, 100000)

# Fill into histogram
for i in x: t_h.Fill(i)

# Save
t_c, t_p = hep.plt.utils.get_canv_pad()

# Adjust height
integral = t_h.Integral()
t_h.Scale(1.0/integral)
hep.plt.utils.adjust_y(t_h)
hep.plt.utils.draw_histos(t_h)
v_axis = hep.plt.utils.add_vertical_axis(0, 1)
h_axis = hep.plt.utils.add_horizontal_axis(0, 1)
v_axis.Draw()
h_axis.Draw()
atlas_tf = hep.plt.utils.get_std_header()
atlas_tf.draw()
hep.plt.utils.add_title(t_h, "Some axis of ordinates [Unit :)]", axis="x")
hep.plt.utils.add_title(t_h, axis="y")
hep.plt.utils.fill_area(t_h)
leg = hep.plt.utils.add_legend(entry=[(t_h, "TH1D entry", "l")])
leg.Draw()
# Add a text box
txt = hep.plt.utils.textFrame(ymax=0.7)
txt.addText("Text")
txt.addText("Text")
txt.addText("Text")
txt.addText("Text")
txt.addText("Text")
txt.addText("Text")
hep.plt.utils.add_grid()
txt.draw()
# Some text on top
tot = hep.plt.utils.text_on_top("This is some text that may easily extend over several lines...;like here...;or here...")
tot.Draw()
# Add a time state (date of creation) to plot
hep.plt.utils.add_time_stamp()
t_c.SaveAs("adjuested_height_lin.pdf")

# Adjust height
t_c.SetLogy()
hep.plt.utils.adjust_y(t_h)
t_h.Draw()
txt.draw()
# Some text on top
tot = hep.plt.utils.text_on_top("Text1;Text2;Text3")
# Add a time state (date of creation) to plot
hep.plt.utils.add_time_stamp()
# Add a grid
hep.plt.utils.add_grid()
tot.Draw()
t_c.SaveAs("adjuested_height_log.pdf")
