import os, sys
project_dir = os.path.abspath(os.path.join(os.path.dirname(__file__)))
sys.path.insert(0, os.path.join(project_dir, ".."))

import hep.root.algo.optimization as optimization
import numpy as np
import ROOT

np.random.seed(0)

# Create a simple histogram
t_h1_sig = ROOT.TH1D("sig", "sig", 100, 0, 1)

# Fill histogram with toy data
data = np.random.normal(1,0.5,1000000)
for d in data: t_h1_sig.Fill(d)

# Save histo
t_c = ROOT.TCanvas("", "", 500, 500)
t_h1_sig.Draw()
t_c.SaveAs("sig.pdf")

# Get cut for working point
cut1_sig_val, cut1_sig_err = optimization.get_cut_on_sig(t_h1_sig, 0.5, "gt")
cut2_sig_val, cut1_sig_err = optimization.get_cut_on_sig(t_h1_sig, 50, "gt")
print("[DUBUG] The signal cut is: %s" % cut1_sig_val)
print("[DUBUG] The signal cut is: %s" % cut2_sig_val)
