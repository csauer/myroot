# Write a tree to a file
from root_numpy import array2root, root2array
import numpy as np

data = np.random.normal(0, 1, size=(10000, 3))
data = [tuple(item) for item in data]

a = np.array(data,
             dtype=[("a", np.float32),
                    ("b", np.float32),
                    ("c", np.float64)])
array2root(a, "test.root", treename="tree", mode="recreate")

