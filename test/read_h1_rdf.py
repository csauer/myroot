import os, sys
project_dir = os.path.abspath(os.path.join(os.path.dirname(__file__)))
sys.path.insert(0, os.path.join(project_dir, ".."))

# Create a FileIo class for ROOT files
import hep.root.reader
root_df = hep.root.reader.set_df("test", "tree", "test.root")

# Define a meta-model for the histogram
import hep.root.utils
t_h1_model = hep.root.utils.get_tH1_model()

# Use multi-threading
hep.root.reader.use_multi_threading()

# Create a FileIo class for ROOT files
import hep.root.fio
root_io = hep.root.fio.FileIo(project_dir)

# Add some files
root_io.addFile("input.root", write_mode="update")
hep.root.reader.set_current_file(root_io.getFile("input.root"), "test/dir")

# Add some histograms to cache
hep.root.reader.add_h1_to_cache("test", "a", t_h1_model, weight="", cut="b>1")
hep.root.reader.add_h1_to_cache("test", "a", t_h1_model, weight="", cut="b>1 && a<0")
hep.root.reader.add_h1_to_cache("test", "a", t_h1_model, weight="", cut="b>1 && c>2")
hep.root.reader.add_h1_to_cache("test", "a", t_h1_model, weight="", cut="b>1 && c*a<2")

# read histgrams
hep.root.reader.read_cache()

# Show content of hist_cache
hep.root.reader.ls_hist_cache()

# Print the current configuration of all global variables in the module root.reader
hep.root.reader.ls_config()
