import os, sys
project_dir = os.path.abspath(os.path.join(os.path.dirname(__file__)))
sys.path.insert(0, os.path.join(project_dir, ".."))

import hep.root.algo.optimization as optimization
import numpy as np
import ROOT

np.random.seed(0)

# Get bkg rej for each bin
n_bin = 1
for std in np.linspace(1,0.5, 10):

  # Create a simple histogram
  t_h1_sig_tot = ROOT.TH1D("sig_tot%s" % n_bin, "sig", 100, 0, 1)
  t_h1_bkg_tot = ROOT.TH1D("bkg_tot%s" % n_bin, "bkg", 100, 0, 1)
  t_h1_sig_tag = ROOT.TH1D("sig_tag%s" % n_bin, "sig", 100, 0, 1)
  t_h1_bkg_tag = ROOT.TH1D("bkg_tag%s" % n_bin, "bkg", 100, 0, 1)
  # Fill histogram with toy data
  data = np.random.normal(1,std,100000)
  for d in data: t_h1_sig_tot.Fill(d)
  data = np.random.normal(0,std,100000)
  for d in data: t_h1_bkg_tot.Fill(d)
  data = np.random.normal(1,std,1000)
  for d in data: t_h1_sig_tag.Fill(d)
  data = np.random.normal(0,std,10000)
  for d in data: t_h1_bkg_tag.Fill(d)
  # get signal eff and bkg rejection
  sig_eff, sig_err, bkg_eff, bkg_err = optimization.eff(t_h1_sig_tot, t_h1_bkg_tot, t_h1_sig_tag, t_h1_bkg_tag)
  n_bin += 1
