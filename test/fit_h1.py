import os, sys
project_dir = os.path.abspath(os.path.join(os.path.dirname(__file__)))
sys.path.insert(0, os.path.join(project_dir, ".."))

import hep.root.algo.optimization as optimization
import hep.root.algo.fit as fit
import numpy as np
import ROOT

np.random.seed(0)

# Histograms for cut
t_h1_cut = ROOT.TH1D("cut", "cut", 10, 0, 1)

n_bin = 1
for std in np.linspace(1,0.5, 10):

  # Create a simple histogram
  t_h1_sig = ROOT.TH1D("sig%s" % n_bin, "sig", 100, 0, 1)
  # Fill histogram with toy data
  data = np.random.normal(1,std,100000)
  for d in data: t_h1_sig.Fill(d)
  # Get cut for working point
  cut_sig_val, cut1_sig_err = optimization.get_cut_on_sig(t_h1_sig, 0.5, "gt")
  t_h1_cut.SetBinContent(n_bin, cut_sig_val)
  n_bin += 1

# Fit this histogram
t_fit = fit.poly1D(t_h1_cut, 5)

# Save histo
t_c = ROOT.TCanvas("", "", 500, 500)
t_h1_cut.Draw()
t_fit.Draw("SAME")
t_c.SaveAs("sig_cut.pdf")
