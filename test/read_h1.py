import os, sys
project_dir = os.path.abspath(os.path.join(os.path.dirname(__file__)))
sys.path.insert(0, os.path.join(project_dir, ".."))

# Create a FileIo class for ROOT files
import hep.root.reader, hep.root.io, hep.root.utils
root_io = hep.root.io.FileIo(os.path.join(project_dir, "out"))

# Add some files
root_io.addFile("input.root")

# Get selction based on cuts and weights
selection = hep.root.utils.get_selection(cut="b>1")

# Define a meta-model for the histogram
import hep.root.utils
t_h1_model = hep.root.utils.get_tH1_model()

# Read histogram
t_h1 = hep.root.reader.get_h1_from_files("test.root", "tree", "a", t_h1_model, selection=selection)

t_c = ROOT.TCanvas("canvas", "", 500, 500)
t_h1.Draw()
t_c.SaveAs("a.pdf")
