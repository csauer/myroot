#!/usr/bin/env python

try:
  import myroot
  print("[TEST] Congratulations, hep is now installed on your system :).")
except ImportError, e:
  print("[WARNING] Module `hep` is not installed on your system. Something went wrong.")
