project_dir = os.path.abspath(os.path.join(os.path.dirname(__file__)))
sys.path.insert(0, os.path.join(project_dir, ".."))

from hep.root.cut import CutStr

# Do a quick test
cut = CutStr()
cut.engulfExpressions()
print(cut)
# Add a cut on a branch in ROOT file
cut.addBranchCut("var1", "var_low1", "var_high1")
cut.addBranchCut("var2", "var_low2",)
cut.addBranchCut("var3", branch_high="var_high3")
print(cut)
# Add some weights
cut.addEventWeight("w1")
cut.addEventWeight("w2")
cut.addEventWeight("w3")
print(cut)
# Add a polynomial cut
cut.addPolyomialCut("x", "y", ["par0", "par1", "par2", "par3", "par4"])
print(cut)
# Finally, get the list of weights and cuts
print(cut.getCutList(), cut.getWeightList())
  
