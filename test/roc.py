import os, sys
project_dir = os.path.abspath(os.path.join(os.path.dirname(__file__)))
sys.path.insert(0, os.path.join(project_dir, ".."))

import hep.root.algo.performance as performance
import hep.plt.derivations
import numpy as np
import ROOT

np.random.seed(0)

# Histograms for cut
t_h1_cut = ROOT.TH1D("cut", "cut", 10, 0, 1)

n_bin = 1
# Create a simple histogram
t_h1_sig = ROOT.TH1D("sig", "sig", 100, 0, 1)
t_h1_bkg = ROOT.TH1D("bkg", "bkg", 100, 0, 1)
# Fill histogram with toy data
data = np.random.normal(1,0.5,100000)
for d in data: t_h1_sig.Fill(d)
data = np.random.normal(0,0.5,100000)
for d in data: t_h1_bkg.Fill(d)

# Get roc curve
roc = performance.roc_curve_binned(t_h1_sig, t_h1_bkg)

# Add plot
hep.plt.derivations.roc_rej_bkg(roc)
